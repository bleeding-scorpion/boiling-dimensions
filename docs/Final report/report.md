---
title: Boiling dimensions
---

# Boiling dimensions Final report

## Abstract

The main parts of the project consists of the implementation of the portal renderer. When looking at a portal surface, the player will be able to see a view borrowed from another part of the world, giving the impression of seing through the surface into a different part of the world.

## Technical aspects

At first, we tried to achieve this result by replacing the texture of the portal surface with a render of the scene as viewed through its portal twin. However, we discover later that this approach was extramely inefficient and we came up with a different rendering method.

The effect was achieved by replacing the portal surface by a rendering done from its linked portal viewpoint. Our implementation is also able to render portal's seen through other portals. Our tests show ~100fps when rendering a simple scene and about ~22fps when rendering a in infinite-recursive scene with max-recursion depth of 1000 (with visually effective depth of about ~300 portals).

When players goes through a portal, they're instantly teleported to that portal's twin. This is done by replacing the player camera pose with the one used to render the portal's surface. This allows us to achieve a nearly seemless transition, modulo the camera angles.

To demonstrate the power of the recursive rendering, we provide 6 scenes varying from a fairly simple pair of portals to 4 cross-linked portals facing each other creating infinite viewports in 4 directions.

## Implementation details

We based our implementation on the code of assignment 9.

The scenes were created in blender and exported as .obj files. These files are then imported using the OBJ-Loader library ([reference](https://github.com/Bly7/OBJ-Loader)) from our application code by specifying a directory containing the scene.obj file and the mtl files referenced by it.

To mark and link portals, their surfaces were given different material names using a predefined pattern with a number in blender. Two portals with the same material name, but different number are then assumed to be linked.

Movement works by having three acceleration direction in the camera. The acceleration in the horizon plane is controlled with the keyboard. The vertical direction is controlled using the scroll wheel. The direction where the camera is pointing is controlled by the mouse.

Collisions are detected by colliding an axis-aligned bounding box (representing the camera) with a bounding box in the portal's canonical base (we calculate and perform the necessary transformation by using the portal plane). 

We first tried to use the *reactphysics3D* library ([reference](https://www.reactphysics3d.com/)) but we finally realized it was outside of the scope of the project and the collision detection could be implemented much easier.

We render the world by scanning all object in the scene and asking them to render themselves, given a camera configuration and a max recursion depth.

We render portals first. When invoked with a depth > 0, they change the OpenGL viewport configuration and start rendering the world from their own point of view; once that render is done, we restore the Viewport configuration and draw the portals again but we replace the color of the portal with a zero-alpha color. The zero-alpha color allows us to use the depth buffer to discard ocluded objects.


## Screenshots

Recursion depth set to 1000
![Recursion depth set to 1000](images/mega_recursion.jpg)

![Recursion depth set to 1000](images/mega_recursion2.jpg)



Example of recursion with 4 portals
![Recursion example](images/recursion_close.jpg)


Simple teleportation
<video width="1080" height="720" controls>
  <source src="images/doors_comp.mp4" type="video/mp4">
</video>


Rotated teleportation
<video width="1080" height="720" controls>
  <source src="images/doors2_comp.mp4" type="video/mp4">
</video>


Recursive teleportation
<video width="1080" height="720" controls>
  <source src="images/recursive_comp.mp4" type="video/mp4">
</video>

## References

- Structure based on assignment 9
- OBJ meshes :
	- Original author : Standford University
	- Source : <http://www.prinmath.com/csci5229/OBJ/index.html>
- OBJ-Loader: <https://github.com/Bly7/OBJ-Loader>
- LodePNG: <https://lodev.org/lodepng/>
- Skybox:
    - Original author: Mr. Who
    - Source: <https://gamebanana.com/textures/5384>

## Running

To run the program, you need to specify a folder containing a scene.obj and the necessary .mtl files as argument. For example:
    `./BoilingDimensions ../assets/sample-scenes/recursive`

Or for a little more fun :
	`./BoilingDimensions ../assets/sample-scenes/find_the_monkey`
