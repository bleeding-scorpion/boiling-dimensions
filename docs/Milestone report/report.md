---
title: Boiling dimensions
---

# Boiling dimensions Milestone report

### What has been done so far

- Different simple scenes in blender to help illustrate our engine.
  These scenes contain material and texture information which will then be exported as obj files.
  The materials information is also used to link the portals directly on blender.

- Simple renderer with a skybox.

### We are currently working on

- Camera movement inside the worldbox when the user presses a key.

- Importing the obj files into our scene by using the library TinyObjLoader.

- Using a library for collision detection with walls and portals.
  We have done some research on different libraries for collision detection
  and we've finally decided on using ReactPhysics3D because it's easy to use
  and enough for our needs.

### Screenshots

![Doors scene in blender](images/doors-scene.png)

![Tunnel scene in blender](images/tunnel-scene.png)

![Renderer in skybox](images/running.gif)


### Updated schedule

Based on the work done so far, here is the updated schedule for the project

Date       | Responsible | Milestone
-----------+----------+------------
May 16th   | Roos     | Parsing the obj files in the program
May 20nd   | Jerémie  | Add collisions detection in the project
May 26th   | Ivan     | Render through portal to a different location
May 28th   | Roos     | Video presentation
May 31th   | Everyone | Final changes and deliverable
