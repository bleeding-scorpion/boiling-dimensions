---
title: Boiling dimensions
---

# Boiling dimensions

We propose a renderer capable of showing a scene containing portals. A portal is a surface on a wall that instantly transports the player to another place in the scene.

# Goals and Deliverables

## Passing grade (4.0)

### Portals

The main parts of the project consists of the implementation of the portal renderer. When looking at a portal surface, the player will be able to see a texture borrowed from another part of the world, giving the impression of seing through the surface into a different part of the world. This effect will be achieved by replacing the texture of the portal surface with a render of the scene as viewed through its portal twin.

![Portal crossing](images/portal-crossing-complete.gif)

In this video (https://www.youtube.com/watch?v=kEB11PQ9Eo8&t=198, at 3:18) you may find visual clues of what we want to achieve. The narrator of the video also explains a similar process for the rendering of portal surfaces, as the one we describe here.

When the player goes through a portal, he'll be instantly teleported to the twin portal position. The teleportation will be achieved by simply changing the camera position to the position of the arrival portal.

## Extensions

### Recursive rendering

The renderer will be able to render a portal that's seen through another portal.

![Recursive portals](images/recursive-portals.gif)

### Scenes

We will also create some scenes to show some interesting effects achievable with portals like the one in the following short clip

![Embedded levels](images/different-levels.gif)

# Schedule

We plan to perform one milestone per week. We assign one responsible for the milestone, which will work with the team and make sure the requirements of said milestone are carried out.

Week       | Responsible | Milestone
-----------+-------------+------------
April 8th  | All         | First proposal
April 22rd | All         | Second proposal
April 29th | All         | Third proposal and project setup and sample scene rendering
May 6th    | Jérémie     | Portal scene + portal rendering
May 13th   | Ivan        | Milestone report + recursive portal rendering
May 20th   | Roos        | Nice scenes
May 27th   | All         | Video presentation + Final report
