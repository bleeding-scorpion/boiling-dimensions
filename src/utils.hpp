#ifndef UTILS_HPP
#define UTILS_HPP

#include <functional>

template<typename T>
class Attribute
{
protected:
    T m_val;
    std::function<T(T)> setter;

public:
    Attribute(const T &a, std::function<T(const T)> _setter = [](T a){ return a; }) {
        m_val = a;
        setter = _setter;
    }

    operator T() const {
        return m_val;
    }

    T &operator=(const T &a) {
        m_val = setter(a);
        return m_val;
    }

    T &operator+=(const T &a) {
        m_val = setter(m_val + a);
        return m_val;
    }

    T get() const {
      return m_val;
    }
};

inline float clamp_to_deg(const float &x) {
  if (x > 360)
    return clamp_to_deg(x - 360);
  if (x < 0)
    return clamp_to_deg(x + 360);
  return x;
}

#endif // UTILS_HPP
