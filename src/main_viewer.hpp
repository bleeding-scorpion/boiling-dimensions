#ifndef MAIN_VIEWER_H
#define MAIN_VIEWER_H

#include <string>

#include "gl.hpp"
#include "glfw_window.hpp"
#include "glmath.hpp"

#include "camera.hpp"
#include "world.hpp"

class Main_viewer : public GLFW_window
{

public:
    Main_viewer(const std::string _title, int _width, int _height, const std::string scene_directory);

    inline void setCamera(Camera& camera_) {
      camera = camera_;
    }

    inline const Camera& getCamera() const {
      return camera;
    }

protected:
    /// function that is called on the creation of the widget for the initialisation of OpenGL
    virtual void initialize();

    /// resize function - called when the window size is changed
    virtual void resize(int width, int height);

    /// paint function - called when the window should be refreshed
    virtual void paint();

    /// keyboard interaction
    virtual void keyboard(int key, int scancode, int action, int mods);

    virtual void mouseMove(double xpos, double ypos);

    virtual void mouseScroll(double xOffset, double yOffset);

    /// function that draws the planet system
    void draw_scene(const Camera& camera);

    /// update function on every timer event (controls the animation)
    virtual void timer();

    /// update the body positions (called by the timer).
    void update_body_positions();


private:
    /// current viewport dimension
    int  width, height;
    Camera camera;
    World world;

    // Usual acceleration step for camera
    float acc_step = 0.8;
    // Mouse to camera movement coefficient
    float mouseSpeed = 0.05;
    // Scroll to camera movement coefficient
    float scrollSpeed = 2;
};

#endif // MAIN_VIEWER_H
