#version 330 core
layout (location = 0) in vec4 v_position;
layout (location = 1) in vec3 v_normal;
layout (location = 2) in vec2 v_texcoord;

out vec2 v2f_texcoord;
out vec3 v2f_light;
out vec3 v2f_normal;
out vec3 v2f_view;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

const vec4 light_position = vec4(50, 100, 0, 1); // in eye space coordinates already

void main()
{
    mat4 mv_matrix = view * model;
    mat4 mvp_matrix = projection * mv_matrix;
    mat3 n_matrix = inverse(transpose(mat3(mv_matrix)));

    //v2f_texcoord = v_texcoord;
    v2f_normal = normalize(n_matrix * v_normal);
    v2f_view = normalize(vec3(mv_matrix * v_position));
    v2f_light = normalize(vec3(light_position) - v2f_view);

    gl_Position = mvp_matrix * v_position;
    v2f_texcoord = gl_Position.xy/gl_Position.z;
}
