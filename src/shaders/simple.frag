#version 330 core
out vec4 f_color;

in vec3 v2f_normal;
in vec2 v2f_texcoord;
in vec3 v2f_light;
in vec3 v2f_view;

const float shininess = 8.0;
const vec3  sunlight = vec3(1.0, 0.941, 0.898);

uniform bool is_portal;

void main()
{
    float n_dot_l = dot(v2f_normal, v2f_light);
    float r_dot_v = dot(reflect(v2f_light, v2f_normal), v2f_view);

    // Extracting texture color
    //vec3 m = texture(tex, v2f_texcoord).rgb;
    vec3 m = vec3(0.4, 0.4, 0.4);

    // Computing Phong lighting
    vec3 color = m * 0.2 * sunlight;
    if (n_dot_l > 0.0) {
        color += sunlight * m * n_dot_l;
        if (r_dot_v > 0.0) {
            color += sunlight * m * pow(r_dot_v, shininess);
        }
    }

    f_color = vec4(color, 1.0);

    if (is_portal)
      f_color = vec4(0, 0, 0, 0);
}
