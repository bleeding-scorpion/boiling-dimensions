//=============================================================================
//
//   Exercise code for "Introduction to Computer Graphics"
//     by Julian Panetta, EPFL
//
//=============================================================================
#version 140

// Eye-space fragment position and normals from vertex shader.
in vec3 v2f_normal;
in vec3 v2f_ec_vertex;

uniform vec3 light_position; // Eye-space light position
uniform vec3 light_color;

// Material parameters
uniform vec3  diffuse_color;
uniform vec3  specular_color;
uniform float shininess;

out vec4 f_color;

void main()
{
    // Normalize the interpolated normal
    vec3 N = -sign(dot(v2f_normal, v2f_ec_vertex)) *  // Orient the normal so it always points opposite the camera rays
             normalize(v2f_normal);

    vec3 color = vec3(0.0f);
    vec3 v2f_view  = normalize(v2f_ec_vertex);
    vec3 v2f_light = normalize(light_position - v2f_ec_vertex);

    float n_dot_l = dot(N, v2f_light);
    float r_dot_v = dot(reflect(v2f_light, N), v2f_view);

    if (n_dot_l > 0) {
        color += light_color * diffuse_color * n_dot_l;
        if (r_dot_v > 0) {
            color += light_color * specular_color * pow(r_dot_v, shininess);
        }
    }

    if(v2f_ec_vertex == vec3(0, 0, 0)){
        f_color = vec4(0, 1.0, 0, .8);
    } else {
        // append the required alpha value
        f_color = vec4(color, 1.0);
    }

}
