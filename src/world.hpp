#ifndef WORLD_HPP
#define WORLD_HPP

#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>

#include "camera.hpp"
#include "glmath.hpp"
#include "Mesh.hpp"
#include "shader.hpp"

class Main_viewer;
class World;

class WorldElement
{
public:
    virtual void initialize() = 0;
    virtual void paint(const Camera& camera, unsigned int recursion_depth) = 0;
    virtual std::string name() = 0;
};

class Skybox : public WorldElement
{
public:
    Skybox();
    void paint(const Camera& camera, unsigned int recursion_depth);
    void initialize();
    std::string name() { return "Skybox"; };
private:
    int textureID;
    Shader worldbox_shader;
    unsigned int skyboxVAO, skyboxVBO;
};

typedef std::pair<std::string, std::string> mesh_key;

class PhysicalObject : public WorldElement
{
public:
    PhysicalObject(const std::string &obj_path, const std::string& object_name, World* world);
    void paint(const Camera& camera, unsigned int recursion_depth);
    void initialize();
    std::string name() { return object_name; };
    mesh_key corresponding_portal(mesh_key portal_key);
private:
    void render_portal(const Camera& camera, unsigned int recursion_depth, const vec4& viewport);
    const std::string obj_path;
    const std::string object_name;
    World* world;
    std::map<std::pair<std::string, std::string>, std::unique_ptr<Mesh>> meshes;
    Shader simple_shader;
    Shader phong_shader;
};

class World
{
public:
    World(const std::string &scene_directory);
    void initialize();
    void paint(const Camera& camera, unsigned int recursion_deptrecursion_depth);
    std::set<std::string> material_ids;
    //std::map<std::string, unsigned int> shared_material_textures;
    std::vector<WorldElement*> world_elements;

    Main_viewer *attached_viewer;
private:
    Skybox skybox;
};

#endif // WORLD_HPP
