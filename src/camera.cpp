#include "camera.hpp"

Camera::Camera(int _width, int _height) :
    // Default values. TODO: Defaults injection?
    center(vec3(0.0, 1.0, 0.0)),
    far(1000), fovy(45), near(0.01),
    x_angle(0, clamp_to_deg), y_angle(0, clamp_to_deg),
    height(_height), width(_width), acceleration_front(0),
    acceleration_side(0), acceleration_top(0)
{
}

void Camera::updatePosition()
{
    if (acceleration_front != 0 || acceleration_side != 0 || acceleration_top != 0) {
        mat3 rot = mat3(mat4::rotate_y(y_angle) * mat4::rotate_x(x_angle));

        vec3 direction_front = rot * vec3(0,0, acceleration_front);

        vec3 direction_side = rot * vec3(acceleration_side, 0, 0);

        vec3 direction_top = rot * vec3(0, acceleration_top, 0);

        center += direction_top + direction_side + direction_front;

        acceleration_front = 0;
        acceleration_side = 0;
        acceleration_top = 0;
    }
}

mat4 Camera::getProjectionMatrix() const
{
    mat4 projection = mat4::perspective(fovy, (float)width/(float)height, near, far);

    //std::cout << "Rendering using params: projection, view:\n" << std::flush;
    //std::cout << projection << std::flush;
    //std::cout << view << std::flush;

    return projection;
}

mat4 Camera::getViewMatrix() const
{
    // Create rotations matrices for the planet or the ship
    mat4 rotate_x = mat4::rotate_x(x_angle);
    mat4 rotate_y = mat4::rotate_y(y_angle);

    vec4 v =     rotate_y * rotate_x * vec4(0.0, 0.0,-1.0, 0.0);
    vec4 up =    rotate_y * rotate_x * vec4(0.0, 1.0, 0.0, 0.0);

    mat4 view = mat4::look_at(center, center + vec3(v), up);
    //std::cout << "Rendering using params: eye, center:" << eye << center << "\n" << std::flush;
    //mat4 view = mat4::rotate_x(0);

    return view;
}
