#include <iostream>

#include "main_viewer.hpp"

int main(int argc, char *argv[])
{
    if (argc < 2) {
      std::cerr << "Please specify scene directory" << std::endl;
      return 0;
    }

    std::cout << "Starting up scene: " << std::string(argv[1]) << std::endl;

    return Main_viewer("Boiling dimensions", 640, 480, argv[1]).run();
}
