#include "main_viewer.hpp"

// Global vars for window/fullscreen toggling
int windowed_xpos, windowed_ypos, windowed_width, windowed_height;

Main_viewer::Main_viewer(const std::string _title, int _width, int _height, const std::string scene_directory)
    : GLFW_window(_title.c_str(), _width, _height), camera(_width, _height), world(scene_directory)
{
}

void Main_viewer::resize(int _width, int _height)
{
    width  = _width;
    height = _height;
    glViewport(0, 0, width, height);
    camera.resize(width, height);
}

void Main_viewer::initialize()
{
    // set initial state
    world.initialize();
    world.attached_viewer = this;
    camera.center = vec3(0);
    camera.x_angle = 0;
    camera.y_angle = 0;
}

void Main_viewer::paint()
{
    glDepthMask(GL_TRUE);
    // clear framebuffer and depth buffer first
    glClearDepth(1.0f);
    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);

    glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    draw_scene(camera);
}

void Main_viewer::draw_scene(const Camera& camera)
{
    glClearColor(0.0, 0.0, 0.0, 1);
    world.paint(camera, 4);

    // check for OpenGL errors
    glCheckError();
}

void Main_viewer::keyboard(int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS || action == GLFW_REPEAT) {
        switch(key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(window_, GL_TRUE);
                break;

            case GLFW_KEY_UP:
            case GLFW_KEY_W:
                camera.acceleration_front += -acc_step;
                break;
            case GLFW_KEY_DOWN:
            case GLFW_KEY_S:
                camera.acceleration_front += acc_step;
                break;
            case GLFW_KEY_LEFT:
            case GLFW_KEY_A:
                camera.acceleration_side += -acc_step;
                break;
            case GLFW_KEY_RIGHT:
            case GLFW_KEY_D:
                camera.acceleration_side += acc_step;
                break;

            case GLFW_KEY_Q:
                camera.y_angle += +45;
                break;

            case GLFW_KEY_E:
                camera.y_angle += -45;
                break;

            case GLFW_KEY_R:
                std::cout << "Center " << camera.center << std::flush;
                std::cout << " x" << camera.x_angle
                          << " y" << camera.y_angle
                          << "\n" << std::flush;
                break;
            case GLFW_KEY_C:
                camera.center = vec3(0);
                camera.x_angle = 0;
                camera.y_angle = 0;
                break;
            case GLFW_KEY_X:
                camera.center = vec3(30, 0, -4.5);
                camera.x_angle = 0;
                camera.y_angle = 110;
                break;
            case GLFW_KEY_Z:
                camera.center = vec3(30, 0, -4.5);
                camera.x_angle = 10;
                camera.y_angle = 80;
                break;

            case GLFW_KEY_T:
#ifndef __APPLE__
                if (glfwGetWindowMonitor(window_))
                {
                    glfwSetWindowMonitor(window_, NULL,
                                 windowed_xpos, windowed_ypos,
                                 windowed_width, windowed_height, 0);
                }
                else
                {
                    GLFWmonitor* monitor = glfwGetPrimaryMonitor();
                    if (monitor)
                    {
                        const GLFWvidmode* mode = glfwGetVideoMode(monitor);
                        glfwGetWindowPos(window_, &windowed_xpos, &windowed_ypos);
                        glfwGetWindowSize(window_, &windowed_width, &windowed_height);
                        glfwSetWindowMonitor(window_, monitor, 0, 0, mode->width, mode->height, mode->refreshRate);
                    }
                }
#endif
                break;
        }
    }
}

void Main_viewer::mouseMove(double xpos, double ypos)
{
    glfwSetCursorPos(window_, width/2., height/2.);

    camera.y_angle += mouseSpeed * (width/2.f - xpos);
    camera.x_angle += mouseSpeed * (height/2.f - ypos);
}

void Main_viewer::mouseScroll(double xOffset, double yOffset)
{
    camera.acceleration_side += xOffset * scrollSpeed;
    camera.acceleration_top += yOffset * scrollSpeed;
}

void Main_viewer::timer()
{
    camera.updatePosition();
}
