#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <algorithm>
#include <cmath>

#include "glmath.hpp"
#include "utils.hpp"

class Camera
{

public:
    Camera(int _width, int _height);
    void resize(int _width, int _height);
    void updatePosition();
    void setAnglesFromNormal(const vec3& normal);
    mat4 getProjectionMatrix() const;
    mat4 getViewMatrix() const;
    vec3 getViewDirection() const;

    Attribute<vec3> center;
    Attribute<float> far, fovy, near;
    Attribute<float> x_angle, y_angle;
    Attribute<float> acceleration_front, acceleration_side, acceleration_top;
    Attribute<int> height, width;
};

inline void Camera::resize(int _width, int _height)
{
    width = _width;
    height = _height;
}

inline void Camera::setAnglesFromNormal(const vec3& normal) {
    vec3 nnx = normalize(vec3(0, normal.y, normal.z));
    vec3 nny = normalize(vec3(normal.x, 0, normal.z));


    float xproj(dot(cross(-vec3(0, 0, 1), nnx), vec3(1, 0, 0)));

    x_angle = rad2deg(std::asin(xproj));

    if (nny.x == 0 && nny.z == 0)
        throw std::runtime_error("Invalid angle transformation");

    y_angle = rad2deg(std::atan2(nny.x, nny.z))-180;
}

inline vec3 Camera::getViewDirection() const {
    mat4 rotation_matrix = /*mat4::rotate_z(z_angle) * */
                           mat4::rotate_y(y_angle) *
                           mat4::rotate_x(x_angle);
    vec4 rotated_in_homogeneous = rotation_matrix * vec4(0, 0, 1, 1);
    rotated_in_homogeneous /= rotated_in_homogeneous.w;
    return vec3(rotated_in_homogeneous);
}

#endif // CAMERA_HPP
