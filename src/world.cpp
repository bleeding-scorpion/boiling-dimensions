#include <sstream>
#include <stdexcept>
#include <cmath>
#include <string>
#include <vector>

#include <OBJ-Loader.h>
#include <lodepng.h>

#include "gl.hpp"
#include "glmath.hpp"

#include "world.hpp"
#include "main_viewer.hpp"

#ifndef ASSETS_PATH
#error "ASSETS_PATH Must be defined"
#endif

static const float skyboxVertices[] = {  // {{{
    // positions
    -1.0f,  1.0f, -1.0f,
    -1.0f, -1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,
     1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,

    -1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,

     1.0f, -1.0f, -1.0f,
     1.0f, -1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,

    -1.0f, -1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,

    -1.0f,  1.0f, -1.0f,
     1.0f,  1.0f, -1.0f,
     1.0f,  1.0f,  1.0f,
     1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f, -1.0f,

    -1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
     1.0f, -1.0f, -1.0f,
     1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
     1.0f, -1.0f,  1.0f
};  // }}}

static unsigned int loadSkyboxTexture()
{
    unsigned int textureID;

    const char* texture_filenames[6] = {
        ASSETS_PATH "/worldbox/sky135ft.png",
        ASSETS_PATH "/worldbox/sky135bk.png",
        ASSETS_PATH "/worldbox/sky135up.png",
        ASSETS_PATH "/worldbox/sky135dn.png",
        ASSETS_PATH "/worldbox/sky135rt.png",
        ASSETS_PATH "/worldbox/sky135lf.png",
    };

    // create texture object
    glGenTextures(1, &textureID);

    unsigned int width, height;

    for (unsigned char i = 0; i < 6; ++i) {
        std::vector<unsigned char> img;

        while (unsigned error = lodepng::decode(img, width, height, texture_filenames[i])) {
            std::cout << "read error: " << lodepng_error_text(error) << std::endl;
            return false;
        }

        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
                0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &img[0]);
        glCheckError();
    }

    // set texture parameters
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    return textureID;
}

static void CheckErrors(std::string desc) {
    GLenum e = glGetError();
    if (e != GL_NO_ERROR) {
        fprintf(stderr, "OpenGL error in \"%s\": %d (%d)\n", desc.c_str(), e, e);
        exit(20);
    }
}

static std::vector<vec3> extractVectices(objl::Mesh mesh) {
    std::vector<vec3> vertices;
    for (auto const& vertice: mesh.Vertices){
        auto p = vertice.Position;
        vertices.emplace_back(p.X, p.Y, p.Z);
    }
    return vertices;
}

static std::vector<Mesh::Face> extractFaces(objl::Mesh mesh) {
    std::vector<Mesh::Face> faces;
    for (int j = 0; j < mesh.Indices.size(); j += 3) {
        faces.emplace_back(mesh.Indices[j], mesh.Indices[j + 1], mesh.Indices[j + 2]);
    }
    return faces;
}

static bool material_is_portal(const std::string& material_id) {
    return material_id.find("Portal") != std::string::npos;
}

static bool check_AABB_collision(std::pair<vec3, vec3> a, std::pair<vec3, vec3> b)
{
    return  (a.second.x <= b.first.x && a.first.x >= b.second.x) &&
            (a.second.y <= b.first.y && a.first.y >= b.second.y) &&
            (a.second.z <= b.first.z && a.first.z >= b.second.z);
}

Skybox::Skybox() : textureID(-1)
{
}

void Skybox::initialize()
{
    glGenVertexArrays(1, &skyboxVAO);
    glGenBuffers(1, &skyboxVBO);
    glBindVertexArray(skyboxVAO);
    glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

    worldbox_shader.load(SHADER_PATH "/worldbox.vert", SHADER_PATH "/worldbox.frag");
    textureID = loadSkyboxTexture();
}

void Skybox::paint(const Camera& camera, unsigned int recursion_depth)
{
    const mat4 projection(camera.getProjectionMatrix()), view(camera.getViewMatrix());
    // We ignore recursion_depth, since the skybox is not recursive
    if (textureID < 0)
        throw std::runtime_error("Requested ID of unitialized texture");
    GLint OldCullFaceMode, OldDepthFuncMode;
    glGetIntegerv(GL_CULL_FACE_MODE, &OldCullFaceMode);
    glGetIntegerv(GL_DEPTH_FUNC, &OldDepthFuncMode);

    glCullFace(GL_FRONT);
    glDepthFunc(GL_LEQUAL);

    worldbox_shader.use();
    worldbox_shader.set_uniform("projection", projection);
    worldbox_shader.set_uniform("view", view);
    worldbox_shader.set_uniform("skybox_texture", (int) textureID);

    glBindTexture(GL_TEXTURE_CUBE_MAP, (unsigned int) textureID);
    glBindVertexArray(skyboxVAO);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);

    worldbox_shader.disable();

    glCullFace(OldCullFaceMode);
    glDepthFunc(OldDepthFuncMode);
}

PhysicalObject::PhysicalObject(const std::string &obj_path_, const std::string& object_name_, World* world_) :
    obj_path(obj_path_), object_name(object_name_), world(world_)
{
}

mesh_key PhysicalObject::corresponding_portal(mesh_key portal_key){
    const std::string portal_name = portal_key.first;
    const std::string portal_material_id = portal_key.second;

    for (auto it=meshes.begin(); it!=meshes.end(); ++it){
        const mesh_key current_key = it->first;
        const std::string current_name = current_key.first;
        const std::string current_material_id = current_key.second;

        if(current_material_id.compare(portal_material_id) == 0 && current_name.compare(portal_name) != 0){
          return current_key;
        }
    }

    fprintf(stderr, "No corresponding portal.");
    exit(20);
}

void PhysicalObject::initialize()
{
    objl::Loader obj;
    obj.LoadFile(obj_path);

    for (auto const& mesh: obj.LoadedMeshes) {
        const std::string &material_id = mesh.MeshMaterial.name;
        const std::string &name = mesh.MeshName;

        world->material_ids.insert(material_id);

        mesh_key key = std::make_pair(name, material_id);
        meshes.insert(std::make_pair(key, std::make_unique<Mesh>(extractVectices(mesh), extractFaces(mesh), material_id, name)));
    }

    if (meshes.size() < 1)
        throw std::runtime_error("Object contains no meshes.");

    // Maybe load texture
    CheckErrors("Preload shader");
    simple_shader.load(SHADER_PATH "/simple.vert", SHADER_PATH "/simple.frag");
    phong_shader.load(SHADER_PATH "/phong.vert",  SHADER_PATH "/phong.frag");
    CheckErrors("Postload shader");
}

void PhysicalObject::paint(const Camera& camera, unsigned int recursion_depth)
{
    const mat4& projection(camera.getProjectionMatrix()), view(camera.getViewMatrix());
    const mat4& model = mat4::identity();  // TODO: Translations?
    const mat4& mv_matrix = view * model;
    const mat4& mvp_matrix = projection * mv_matrix;
    const mat3& n_matrix = inverse(transpose(mat3(mv_matrix)));

    int viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);  // (lower_left) x, y, width, height
    vec4 curr_viewport(viewport[0], viewport[1], viewport[2], viewport[3]);

    vec3 curr_viewport_origin(curr_viewport[0], curr_viewport[1], 0);
    vec3 curr_viewport_size(curr_viewport[2], curr_viewport[3], 1);

    if (curr_viewport_size.x < 4 || curr_viewport_size.y < 4)  // Skip too-small viewports
        return;

    std::vector<mesh_key> drawn_mesh_keys;

    for (auto it=meshes.begin(); it!=meshes.end(); ++it){
        const auto& mesh = it->second;

        // Render only portals
        if (!material_is_portal(mesh->material_id))
            continue;

        if (recursion_depth > 0) {
            vec3 bot_l(std::numeric_limits<double>::max());
            vec3 top_r(std::numeric_limits<double>::lowest());
            double max_unprojected_z = std::numeric_limits<double>::lowest();

            for (const auto& v: mesh->vertices_) {
                vec4 pos = mv_matrix * vec4(v.position, 1);
                pos /= pos.w; // Normalize homogeneous
                if (pos.z > max_unprojected_z)
                    max_unprojected_z = pos.z;

                // Project
                pos = projection * pos;
                pos /= pos.w; // Normalize homogeneous
                bot_l = min(bot_l, vec3(pos));
                top_r = max(top_r, vec3(pos));
            }

            vec3 portal_normal = normalize(mesh->getNormal());
            vec3 camera_normal = normalize(camera.getViewDirection());

            if (max_unprojected_z > 0)  // Mesh is “too” behind viewport. FIXME: fov
                continue;

            if (dot(portal_normal, camera_normal) < -0.5)  // Mesh is facing “too” away from the camera. FIXME: fov
                continue;

            // Transform from view cube coords to viewport coords
            bot_l = curr_viewport_origin + curr_viewport_size * (bot_l + vec3(1))/2;
            top_r = curr_viewport_origin + curr_viewport_size * (top_r + vec3(1))/2;

            bot_l.x = std::floor(bot_l.x);
            bot_l.y = std::floor(bot_l.y);
            top_r.x = std::ceil(top_r.x);
            top_r.y = std::ceil(top_r.y);

            vec3 span = top_r - bot_l;
            vec4 native_portal_viewport(bot_l.x, bot_l.y, span.x, span.y);  // vierport: (lower_left) x, y, width, height

            mesh_key corr_portal_key = corresponding_portal(it->first);
            const auto& corr_mesh = meshes.find(corr_portal_key)->second;
            vec3 corr_mesh_normal = normalize(corr_mesh->getNormal());
            vec3 corr_mesh_center = corr_mesh->getCenter();

            vec4 portal_viewport;
            portal_viewport[0] = std::max(int(native_portal_viewport[0]), viewport[0]);
            portal_viewport[1] = std::max(int(native_portal_viewport[1]), viewport[1]);
            int removed_width (std::abs(portal_viewport[0] - native_portal_viewport[0]));
            int removed_height(std::abs(portal_viewport[1] - native_portal_viewport[1]));

            portal_viewport[2] = std::min(int(native_portal_viewport[2]-removed_width ), viewport[2]);
            portal_viewport[3] = std::min(int(native_portal_viewport[3]-removed_height), viewport[3]);

            if (portal_viewport[2] < 1  || portal_viewport[3] < 1)  // If portal_viewport size is too small
                continue;

            Camera linked_portal_camera(portal_viewport[2], portal_viewport[3]);
            linked_portal_camera.center  = corr_mesh_center;
            linked_portal_camera.setAnglesFromNormal(corr_mesh_normal);

            glClear(GL_DEPTH_BUFFER_BIT);
            render_portal(linked_portal_camera, recursion_depth, portal_viewport);
            drawn_mesh_keys.push_back(it->first);
            glClearDepth(1.0f);
            glClear(GL_DEPTH_BUFFER_BIT);

            vec3 mesh_center = mesh->getCenter();
            mat4 matrix = mat4::look_at(camera.center, mesh_center, vec3(0, -1, 0)) * mat4::translate(mesh_center); 

            std::pair<vec3, vec3> mesh_aabb = mesh->axis_aligned_bounding_box();
            std::pair<vec3, vec3> cam_aabb = {camera.center + vec3(0.5), camera.center + vec3(0.5)};

            mesh_aabb.first  = vec3(matrix * vec4(mesh_aabb.first, 0)) + vec3(0, 0, 2.5);
            mesh_aabb.second = vec3(matrix * vec4(mesh_aabb.second, 0)) - vec3(0, 0, 2.5);

            cam_aabb.first  = vec3(matrix * vec4(cam_aabb.first, 0));
            cam_aabb.second = vec3(matrix * vec4(cam_aabb.second, 0));

            if (check_AABB_collision(mesh_aabb, cam_aabb))
            {
                std::cout << "Teleporting from " << camera.center
                            << " To " << linked_portal_camera.center
                            << "\n" << std::flush;
                linked_portal_camera.resize(viewport[2], viewport[3]);
                world->attached_viewer->setCamera(linked_portal_camera);
            }

            matrix = mat4::look_at(mesh_center, camera.center, vec3(0, -1, 0)) * mat4::translate(mesh_center); 

            mesh_aabb = mesh->axis_aligned_bounding_box();
            cam_aabb = {camera.center + vec3(0.5), camera.center + vec3(0.5)};

            mesh_aabb.first  = vec3(matrix * vec4(mesh_aabb.first, 0)) + vec3(0, 0, 2.5);
            mesh_aabb.second = vec3(matrix * vec4(mesh_aabb.second, 0)) - vec3(0, 0, 2.5);

            cam_aabb.first  = vec3(matrix * vec4(cam_aabb.first, 0));
            cam_aabb.second = vec3(matrix * vec4(cam_aabb.second, 0));

            if (check_AABB_collision(mesh_aabb, cam_aabb))
            {
                std::cout << "Teleporting from " << camera.center
                            << " To " << linked_portal_camera.center
                            << "\n" << std::flush;
                linked_portal_camera.resize(viewport[2], viewport[3]);
                world->attached_viewer->setCamera(linked_portal_camera);
            }
        }
    }

    for (auto &drawn_portal_mesh_key : drawn_mesh_keys){
        const auto& mesh = meshes[drawn_portal_mesh_key];

        // Render only portals
        if (!material_is_portal(mesh->material_id))
            continue;

        CheckErrors("Predraw");
        simple_shader.use();
        simple_shader.set_uniform("model", model);
        simple_shader.set_uniform("view", view);
        simple_shader.set_uniform("projection", projection);
        simple_shader.set_uniform("is_portal", true);

        mesh->draw(GL_TRIANGLES);

        simple_shader.disable();
        CheckErrors("Postdraw");
    }

    for (auto it=meshes.begin(); it!=meshes.end(); ++it){
        const auto& mesh = it->second;

        // Render everything else
        if (material_is_portal(mesh->material_id))
            continue;

        vec3 diffuse (.5, .5, .5), // used as ambient color too
             specular(0.2, 0.2, 0.2);

        CheckErrors("Predraw");
        phong_shader.use();

        // For vertex shader
        phong_shader.set_uniform("modelview_projection_matrix", mvp_matrix);
        phong_shader.set_uniform("modelview_matrix", mv_matrix);
        phong_shader.set_uniform("normal_matrix", n_matrix);

        // For fragment shader
        phong_shader.set_uniform("light_position", vec3(view * vec3(0, 20, 0)));
        phong_shader.set_uniform("light_color", vec3(.7, .7, .7));
        phong_shader.set_uniform("diffuse_color", diffuse);
        phong_shader.set_uniform("specular_color", specular);
        phong_shader.set_uniform("shininess", 50.0f, true);

        mesh->draw(GL_TRIANGLES);

        phong_shader.disable();
        CheckErrors("Postdraw");
    }
}

void PhysicalObject::render_portal(const Camera& camera, unsigned int recursion_depth, const vec4& viewport) {
    if (recursion_depth == 0)
        throw std::runtime_error("Request to render a bottom portal...");

    int original_viewport[4];
    glGetIntegerv(GL_VIEWPORT, original_viewport);
    glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);
    CheckErrors("Predraw inner world");
    world->paint(camera, recursion_depth - 1);
    CheckErrors("Postdraw inner world");
    glViewport(original_viewport[0], original_viewport[1], original_viewport[2], original_viewport[3]);
}


World::World(const std::string &scene_directory)
{
    world_elements.push_back(new PhysicalObject(scene_directory + "/scene.obj", "Scene", this));
    world_elements.push_back(new Skybox());
}

void World::initialize()
{
    glDepthMask(GL_TRUE);
    glEnable(GL_DEPTH_TEST);
    std::cout << "Initializing " << world_elements.size() << " elements\n" << std::flush;
    for(WorldElement* world_element_p : world_elements) {
        const std::string& element_name = world_element_p->name();
        if (!element_name.empty())
            std::cout << "Initializing " << element_name << "\n" << std::flush;
        world_element_p->initialize();
    }
}

void World::paint(const Camera& camera, unsigned int recursion_depth)
{
    glDisable(GL_BLEND);
    for (WorldElement* world_element_p : world_elements) {
        world_element_p->paint(camera, recursion_depth);
    }
    glEnable(GL_BLEND);
}
